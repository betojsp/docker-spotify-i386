FROM debian:jessie
MAINTAINER betojsp

# Install Spotify and PulseAudio.
WORKDIR /usr/src
RUN apt-get update \
	&& apt-get install -y --no-install-recommends gnupg \
	&& apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D1742AD60D811D58 \
	&& echo "deb http://repository.spotify.com testing non-free" > /etc/apt/sources.list.d/spotify.list \
	&& apt-get update \
	&& apt-get install -y --no-install-recommends --allow-unauthenticated \
		spotify-client xdg-utils libxss1 \
		pulseaudio libgl1-mesa-glx \
		fonts-noto \
	&& apt-get clean \
	&& apt-get autoremove \
	&& rm -rf /var/lib/apt/lists/* \
	&& echo enable-shm=no >> /etc/pulse/client.conf \
	&& rm -rf /var/lib/apt/lists/*

# Spotify data.
VOLUME ["/data/cache", "/data/config"]
WORKDIR /data
RUN mkdir -p /data/cache \
	&& mkdir -p /data/config

# PulseAudio server.
ENV PULSE_SERVER /run/pulse/native

COPY docker-entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

CMD ["spotify"]